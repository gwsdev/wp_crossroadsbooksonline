<?php if(!defined('ABSPATH')) exit; // Exit if accessed directly

$currency_options = apply_filters('wc_aelia_currencyswitcher_widget_currency_options', $this->get_currency_options());

/**
 * Renders the options for the Dropdown field that allows to change currency.
 *
 * @param array currency_options An array of Currency => Name pairs.
 * @param string selected_currency The currently selected Currency.
 */
if(!function_exists('AeliaCS_RenderCurrencyOptions')) {
	function AeliaCS_RenderCurrencyOptions($currency_options, $selected_currency) {
		foreach($currency_options as $CurrencyCode => $CurrencyName) {
			$Selected = ($CurrencyCode === $selected_currency) ? 'selected="selected"' : '';
			echo '<option value="' . $CurrencyCode . '" ' . $Selected . '>' . $CurrencyName. '</option>';
		}
	}
}

$selected_currency = WC_Aelia_CurrencySwitcher::instance()->get_selected_currency();

echo $before_widget;

// This wrapper is needed for widget JavaScript to work correctly
echo '<div class="widget_wc_aelia_currencyswitcher_widget">';

// Variable $currency_switcher_widget_title is set in WC_Aelia_CurrencySwitcher_Widget::widget()
if(!empty($currency_switcher_widget_title)) {
	echo $before_title . $currency_switcher_widget_title . $after_title;
}

// If one or more Currencies are misconfigured, inform the Administrators of
// such issue
if((get_value('misconfigured_currencies', $this, false) === true) && current_user_can('manage_options')) {
	$error_message = WC_Aelia_CurrencySwitcher::instance()->get_error_message(AELIA_CS_ERR_MISCONFIGURED_CURRENCIES);
	echo '<div class="error">';
	echo '<h5 class="title">' . __('Error', AELIA_CS_PLUGIN_TEXTDOMAIN) . '</h5>';
	echo $error_message;
	echo '</div>';
}

echo '<!-- Currency Switcher v.' . WC_Aelia_CurrencySwitcher::VERSION . ' - Currency Selector Widget -->';
echo '<form method="post" class="currency_switch_form">';
echo '<select id="aelia_cs_currencies" name="' . AELIA_CS_ARG_CURRENCY . '">';
AeliaCS_RenderCurrencyOptions($currency_options, $selected_currency);
echo '</select>';
echo '<button type="submit" class="button change_currency">' . __('Change Currency', AELIA_CS_PLUGIN_TEXTDOMAIN) . '</button>';
echo '</form>';

echo '</div>';

echo $after_widget;
