<?php
namespace Aelia\CurrencySwitcher;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use \WC_Aelia_CurrencySwitcher;

class API {
	protected static $instance;

	public static function get_enabled_currencies() {
		return WC_Aelia_CurrencySwitcher::settings()->get_enabled_currencies();
	}

	public static function enabled_currencies() {

	public static function instance() {
		if(empty(self::$instance)) {
			self::$instance = API::factory();
		}

		return self::$instance;
	}

	public static function factory() {
		$instance = new API();
		return $instance;
	}

	public function __construct() {
		$this->set_hooks();
	}

	protected function set_hooks() {
		// Add a filter for 3rd parties to retrieve the list of enabled currencies
		add_filter('wc_aelia_cs_enabled_currencies', array($this, 'wc_aelia_cs_enabled_currencies'));
	}

	/**
	 * Returns a list of enabled currencies.
	 */
	public function wc_aelia_cs_enabled_currencies($default) {
		return self::enabled_currencies();
	}
}
