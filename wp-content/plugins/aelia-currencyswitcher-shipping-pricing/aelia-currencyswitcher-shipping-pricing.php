<?php if(!defined('ABSPATH')) exit; // Exit if accessed directly
/*
Plugin Name: Aelia Shipping Pricing for Currency Switcher
Description: Adds the possibility of specifying shipping prices for each currency, rather than rely on automatic conversion.
Author: <a href="http://aelia.co">Aelia</a>
Version: 1.1.4.150213
*/

require_once dirname(__FILE__) . '/src/lib/classes/install/aelia-wc-cs-shipping-pricing-requirementscheck.php';
// If requirements are not met, deactivate the plugin
if(Aelia_WC_ShippingPricing_RequirementsChecks::factory()->check_requirements()) {
	require_once dirname(__FILE__) . '/src/plugin-main.php';
}
