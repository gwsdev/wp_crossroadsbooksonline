# Aelia Shipping Pricing for Currency Switcher

## Version 1.x
####1.1.4.150213
* Fixed bug in logic used to determine if a shipping method is enabled.

####1.1.3.150213
* Fixed notice that appeared when saving the settings for a shipping method.

####1.1.2.141002
* Renamed flag that indicates if shipping prices are already in active currency.
  Required for compatibility with Currency Switcher 3.5.4.141002 and later.
* Updated requirements.

####1.1.1.140819
* Updated logic used to for requirements checking.

####1.1.0.140724
* Fixed check to determine if a shipping method is enabled.
* Fixed check to determine if the prices entered for a shipping method should be used.
* Added preliminary support for Bolder Elements Table Rate Shipping.

####1.0.1.140721
* Updated messages for missing requirements.

####1.0.0.140718
* First public release.

## Version 0.x
####0.8.0.140717-beta
* Modified shipping method settings UI:
	* Added currency selector, to easily switch between prices.
	* Modified labels and descriptions for extra currencies.
* Added explicit list of supported shipping methods, to avoid overriding unsupported ones.
* Fixed bug in class Aelia_CS_WC_Shipping_Flat_Rate:
	* Corrected logic used to load flat rate rules for each currency.

####0.7.0.140714-beta
* Added logic to allow explicit declaration of shipping methods, to replace auto-generated classes.
* Added new filters:
	* Added `aelia_cs_shippingpricing_shipping_methods_with_autogen`, to allow manipulation of auto-generated classes **before** explicit overrides are applied.
	* Added `aelia_cs_shippingpricing_shipping_methods_with_overrides`, to allow manipulation of auto-generated classes **after** explicit overrides are applied.
* Moved filter for `woocommerce_shipping_methods` hook to priority 15, to allow other plugins to register their own shipping methods.

####0.5.1.140711-alpha
* Added logic to replace all shipping methods with dynamically generated ones.

####0.5.0.140711-alpha
* Completed first implementation of shipping method template:
	* Renamed class to `Aelia_Shipping_Method_Template`.
	* Overridden init_settings() method, to load settings in the correct currency.
	* Overridden process_admin_options() method, to save settings in the correct currency.
	* Overridden admin_options() method, to render shipping method's settings page.
	* Added generate_hidden_html() method, to render a an input with `type="hidden"`.
	* Improved logic used to determine when to use explicit prices in currency, and when have them converted using exchange rates.
* Extended WC_Aelia_CS_ShippingPricing_Plugin class:
	* Implemented dynamic replacement of shipping method classes.
	* Added new filter `aelia_cs_shippingpricing_generated_class`.

####0.2.0.140711-alpha
* Improved plugin loader file. Added explicit path to load main plugin class.
* Added stub installer class.
* Added shipping method template class.
* Disabled loading of unneeded settings classes.

####0.1.0.140711-alpha
* First plugin draft.
