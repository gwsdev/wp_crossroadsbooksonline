<?php
namespace Aelia\WC\CurrencySwitcher\ShippingPricing;
if(!defined('ABSPATH')) exit; // Exit if accessed directly

//define('SCRIPT_DEBUG', 1);
//error_reporting(E_ALL);

require_once('lib/classes/definitions/definitions.php');

use Aelia\WC\Aelia_Plugin;
use Aelia\WC\Aelia_SessionManager;
use Aelia\WC\CurrencySwitcher\ShippingPricing\Settings;
use Aelia\WC\CurrencySwitcher\ShippingPricing\Settings_Renderer;
use Aelia\WC\CurrencySwitcher\ShippingPricing\Messages;

/**
 * Main plugin class.
 **/
class WC_Aelia_CS_ShippingPricing_Plugin extends Aelia_Plugin {
	public static $version = '1.1.4.150213';

	public static $plugin_slug = Definitions::PLUGIN_SLUG;
	public static $text_domain = Definitions::TEXT_DOMAIN;
	public static $plugin_name = 'Aelia Currency Switcher - Shipping Pricing';

	// @var bool Indicates if the settings page of a shipping method is being rendered
	protected static $_processing_settings = false;

	// @var string Contains the declaration of the template class that will be used to extend shipping methods.
	protected $_shipping_method_template;

	// @var array A list of the shipping methods supported by the plugin.
	// It will be used to ensure that only the supported methods will be overridded by the plugin
	protected $_supported_shipping_methods = array(
		'WC_Shipping_Flat_Rate',
		'WC_Shipping_Free_Shipping',
		'WC_Shipping_International_Delivery',
		'WC_Shipping_Local_Delivery',
		'WC_Shipping_Local_Pickup',
		// Bolder Elements - Table Rate Shipping
		'BE_Table_Rate_Shipping',
	);

	/**
	 * Indicates if the settings page of a shipping method is being rendered, and
	 * which page.
	 *
	 * @return false|string
	 */
	public static function processing_settings() {
		return self::$_processing_settings;
	}

	/**
	 * Factory method.
	 */
	public static function factory() {
		// Load Composer autoloader
		require_once(__DIR__ . '/vendor/autoload.php');

		$settings_key = self::$plugin_slug;

		// Settings and messages classes are loaded from the same namespace as the
		// plugin
		//$settings_page_renderer = new Settings_Renderer();
		//$settings_controller = new Settings(Settings::SETTINGS_KEY,
		//																		self::$text_domain,
		//																		$settings_page_renderer);
		$messages_controller = new Messages();

		$class = get_called_class();
		$plugin_instance = new $class(null, $messages_controller);
		return $plugin_instance;
	}

	/**
	 * Constructor.
	 *
	 * @param \Aelia\WC\Settings settings_controller The controller that will handle
	 * the plugin settings.
	 * @param \Aelia\WC\Messages messages_controller The controller that will handle
	 * the messages produced by the plugin.
	 */
	public function __construct($settings_controller = null,
															$messages_controller = null) {
		// Load Composer autoloader
		require_once(__DIR__ . '/vendor/autoload.php');

		parent::__construct($settings_controller, $messages_controller);
	}

	/**
	 * Sets the hooks required by the plugin.
	 */
	protected function set_hooks() {
		parent::set_hooks();

		add_filter('woocommerce_shipping_methods', array($this, 'woocommerce_shipping_methods'), 15, 1);
		add_action('woocommerce_settings_shipping', array($this, 'set_processing_shipping_flag'), 1);
		add_action('woocommerce_settings_save_shipping', array($this, 'set_processing_shipping_flag'), 1);
	}

	/**
	 * Alters the list of the shipping methods passed to WooCommerce.
	 *
	 * @param array shipping_methods The array of shipping methods.
	 * @return array
	 */
	public function woocommerce_shipping_methods($shipping_methods) {
		$new_shipping_methods = array();

		// Replace all shipping classes with dyniamcally generated ones
		foreach($shipping_methods as $original_class) {
			if(in_array($original_class, $this->_supported_shipping_methods)) {
				// If shipping method is amongst the supported one, override its class
				$new_shipping_methods[] = $this->generate_shipping_method_class($original_class);
			}
			else {
				// Leave unsupported shipping methods untouched, to prevent unexpected side effects
				$new_shipping_methods[] = $original_class;
			}
		}

		$new_shipping_methods = apply_filters('aelia_cs_shippingpricing_shipping_methods_with_autogen', $new_shipping_methods);
		// Override auto-generated shipping methods with explicitly declared classes
		$new_shipping_methods = $this->load_shipping_methods_overrides($new_shipping_methods);
		$new_shipping_methods = apply_filters('aelia_cs_shippingpricing_shipping_methods_with_overrides', $new_shipping_methods);

		// Debug
		//var_dump($new_shipping_methods);

		return $new_shipping_methods;
	}

	/**
	 * Invoked when the settings page of a shipping method is invoked. This method
	 * keeps track of when a settings page is rendered, so that the shipping methods
	 * ae aware of it.
	 */
	public function set_processing_shipping_flag() {
		self::$_processing_settings = true;
	}

	/**
	 * Determines if one of plugin's admin pages is being rendered. Override it
	 * if plugin implements pages in the Admin section.
	 *
	 * @return bool
	 */
	protected function rendering_plugin_admin_page() {
		$screen = get_current_screen();
		$page_id = $screen->id;

		return ($page_id == 'woocommerce_page_' . Definitions::MENU_SLUG);
	}

	/**
	 * Registers the script and style files needed by the admin pages of the
	 * plugin. Extend in descendant plugins.
	 */
	protected function register_plugin_admin_scripts() {
		// Scripts
		wp_register_script('jquery-ui',
											 '//code.jquery.com/ui/1.10.3/jquery-ui.js',
											 array('jquery'),
											 null,
											 true);
		wp_register_script('chosen',
											 '//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js',
											 array('jquery'),
											 null,
											 true);

		// Styles
		wp_register_style('chosen',
												'//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css',
												array(),
												null,
												'all');
		wp_register_style('jquery-ui',
											'//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css',
											array(),
											null,
											'all');

		wp_enqueue_style('jquery-ui');
		wp_enqueue_style('chosen');

		wp_enqueue_script('jquery-ui');
		wp_enqueue_script('chosen');

		parent::register_plugin_admin_scripts();
	}

	/**
	 * Loads and returns the declaration of the class that will be used to create
	 * multi-currency shipping method classes on the fly.
	 *
	 * @return string
	 */
	protected function shipping_method_template() {
		if(empty($this->_shipping_method_template)) {
			$this->_shipping_method_template = file_get_contents($this->path('classes') . '/shipping/shipping_method_template.php');
			// Remove the leading "<?php" tag, not needed for the eval()
			$this->_shipping_method_template = preg_replace('/<\?php/', '', $this->_shipping_method_template, 1);
		}
		return $this->_shipping_method_template;
	}

	/**
	 * Generates a shipping method class on the fly, extending the original class
	 * by adding support for multiple currencies. This method also loads the class,
	 * so that it can be used elsewhere.
	 *
	 * @param string original_class The original class to extend.
	 * @return string The name of the generated class.
	 */
	protected function generate_shipping_method_class($original_class) {
		$class_template = $this->shipping_method_template();

		// Generate a new shipping class on the fly, extending the one passed as a
		// parameter
		$new_class_name = Definitions::SHIPPING_METHOD_PREFIX . $original_class;
		if(!class_exists($new_class_name)) {
			$class = str_replace(Definitions::PLACEHOLDER_SHIPPING_METHOD,
													 $new_class_name . ' extends \\' . $original_class,
													 $class_template);
			eval($class);

			// Debug
			//var_dump($new_class_name, class_exists($new_class_name), $class);die();
		}

		// Allow for explicitly declared classes to replace the one that was just generated
		$new_class_name = apply_filters('aelia_cs_shippingpricing_generated_class', $new_class_name, $original_class);

		return $new_class_name;
	}

	/**
	 * Replaces some of the auto-generated shipping methods with ones that require
	 * or contain extended functionalities, which an auto-generated class cannot
	 * provide.
	 *
	 * @param array shipping_methods An array of shipping method classes.
	 * @return array
	 */
	protected function load_shipping_methods_overrides($shipping_methods) {
		$class_substitution_map = array(
			'Aelia_CS_AutoGen_WC_Shipping_Flat_Rate' => 'Aelia_CS_WC_Shipping_Flat_Rate',
			// Bolder Elements - Table Rate Shipping
			'Aelia_CS_AutoGen_BE_Table_Rate_Shipping' => 'Aelia_CS_WC_Shipping_BE_Table_Rate_Shipping',
		);

		$result = array();
		foreach($shipping_methods as $shipping_method_class) {
			// If a class is to be replaced, and the replacement class exists, perform
			// the replacement
			if(isset($class_substitution_map[$shipping_method_class]) &&
				 class_exists($class_substitution_map[$shipping_method_class])) {
				$result[] = $class_substitution_map[$shipping_method_class];
			}
			else {
				$result[] = $shipping_method_class;
			}
		}

		return $result;
	}

	/**
	 * Registers the script and style files required in the backend (even outside
	 * of plugin's pages). Extend in descendant plugins.
	 */
	protected function register_common_admin_scripts() {
		parent::register_common_admin_scripts();

		// Admin styles
		wp_register_style(static::$plugin_slug . '-admin',
											$this->url('plugin') . '/design/css/admin.css',
											array(),
											null,
											'all');
		wp_enqueue_style(static::$plugin_slug . '-admin');
	}
}

$GLOBALS[WC_Aelia_CS_ShippingPricing_Plugin::$plugin_slug] = WC_Aelia_CS_ShippingPricing_Plugin::factory();
