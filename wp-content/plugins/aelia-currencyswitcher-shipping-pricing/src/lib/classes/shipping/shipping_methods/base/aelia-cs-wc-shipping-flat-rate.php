<?php
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use Aelia\WC\CurrencySwitcher\ShippingPricing\Definitions;
use Aelia\WC\CurrencySwitcher\ShippingPricing\WC_Aelia_CS_ShippingPricing_Plugin;
use \WC_Aelia_CurrencySwitcher;

/**
 * Extends the auto-generated class for the Flat Rate shipping method included
 * in WooCommerce.
 */
class Aelia_CS_WC_Shipping_Flat_Rate extends Aelia_CS_AutoGen_WC_Shipping_Flat_Rate {

	/**
	 * Generates the settings key for a the flat rates option.
	 *
	 * @param string currency The target currency.
	 * @return string
	 */
	protected function get_flat_rate_option($currency) {
		$settings_key = 'woocommerce_flat_rates';
		if(!empty($currency)) {
			$settings_key .= '_' . $currency;
		}
		return $settings_key;
	}

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->method_title = str_replace(' (CS)', ' (CSX)', $this->method_title);
	}

	/**
	 * Initialises the shipping method.
	 */
	public function init() {
		$this->flat_rate_option = $this->get_flat_rate_option($this->active_currency());

		parent::init();
	}

	/**
	 * Retrieves the flat rates rules. This method takes into account the flat
	 * rates explicitly defined for the active currency, falling back to automatic
	 * conversion when manual prices are configured to be ignored.
	 */
	public function get_flat_rates() {
		parent::get_flat_rates();

		// If viewing the settings page for this shipping method, don't do anything
		// else and keep the settings as they are
		if(is_admin() && WC_Aelia_CS_ShippingPricing_Plugin::processing_settings()) {
			return;
		}

		// If we reach this point, we need to determine if we should use the flat
		// rates for the specific currency, or fall back to the default pricing,
		// to be converted using exchange rates
		if(!$this->manual_prices_enabled) {
			$base_flat_rate_option = $this->get_flat_rate_option($this->base_currency());
			$this->flat_rates = array_filter((array)get_option($base_flat_rate_option));
		}
	}
}
