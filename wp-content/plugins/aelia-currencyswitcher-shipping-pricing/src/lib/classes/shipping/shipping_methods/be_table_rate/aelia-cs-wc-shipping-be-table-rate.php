<?php
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use Aelia\WC\CurrencySwitcher\ShippingPricing\Definitions;
use Aelia\WC\CurrencySwitcher\ShippingPricing\WC_Aelia_CS_ShippingPricing_Plugin;
use \WC_Aelia_CurrencySwitcher;

/**
 * Extends the auto-generated class for the Flat Rate shipping method included
 * in WooCommerce.
 */
class Aelia_CS_WC_Shipping_BE_Table_Rate_Shipping extends Aelia_CS_AutoGen_BE_Table_Rate_Shipping {

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->method_title = str_replace(' (CS)', ' (CSX)', $this->method_title);
	}

	/**
	 * Initialises the shipping method.
	 */
	public function init() {
		$options_to_override = array(
			'table_rate_options' => 'woocommerce_table_rates',
			'class_priorities_options' => 'woocommerce_class_priorities',
			'handling_rates_options'  => 'woocommerce_handling_rates',
			'title_order_options'  => 'woocommerce_trshipping_title_orders',
		);

		foreach($options_to_override as $option => $option_key) {
			$this->$option = $this->get_option_key($option_key, $this->active_currency());
		}

		parent::init();
	}

	/**
	 * Retrieves the table rates.
	 * This method takes into account the settings explicitly defined for the
	 * active currency, falling back to automatic conversion when manual prices
	 * are configured to be ignored.
	 */
	public function get_table_rates() {
		parent::get_table_rates();

		if($this->should_load_base_currency_settings()) {
			$base_currency_option = $this->get_option_key('woocommerce_table_rates', $this->base_currency());
			$this->table_rates = array_filter((array)get_option($base_currency_option));
		}
	}

	/**
	 * Retrieves the class priorities.
	 * This method takes into account the settings explicitly defined for the
	 * active currency, falling back to automatic conversion when manual prices
	 * are configured to be ignored.
	 */
	public function get_class_priorities() {
		parent::get_class_priorities();

		if($this->should_load_base_currency_settings()) {
			$base_currency_option = $this->get_option_key('woocommerce_class_priorities', $this->base_currency());
			$this->class_priorities = array_filter((array)get_option($base_currency_option));
		}
	}

	/**
	 * Retrieves the handling rates.
	 * This method takes into account the settings explicitly defined for the
	 * active currency, falling back to automatic conversion when manual prices
	 * are configured to be ignored.
	 */
	public function get_handling_rates() {
		parent::get_handling_rates();

		if($this->should_load_base_currency_settings()) {
			$base_currency_option = $this->get_option_key('woocommerce_handling_rates', $this->base_currency());
			$this->handling_rates = array_filter((array)get_option($base_currency_option));
		}
	}

	/**
	 * Retrieves the title order options.
	 * This method takes into account the settings explicitly defined for the
	 * active currency, falling back to automatic conversion when manual prices
	 * are configured to be ignored.
	 */
	public function get_title_order() {
		parent::get_title_order();

		if($this->should_load_base_currency_settings()) {
			$base_currency_option = $this->get_option_key('woocommerce_trshipping_title_orders', $this->base_currency());
			$this->title_order = array_filter((array)get_option($base_currency_option));
		}
	}

	/**
	 * Returns the path where the Admin Views can be found.
	 *
	 * @return string
	 */
	protected function admin_views_path() {
		return WC_Aelia_CS_ShippingPricing_Plugin::plugin_path() . '/views/admin/shipping_methods/be_table_rate';
	}

	/**
	 * Renders the settings screen.
	 */
	public function admin_options() {
		$this->load_settings_page_scripts();
		include($this->admin_views_path() . '/admin_options.php');
	}
}
