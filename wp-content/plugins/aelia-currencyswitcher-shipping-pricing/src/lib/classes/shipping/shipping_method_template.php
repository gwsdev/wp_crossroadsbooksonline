<?php
if(!defined('ABSPATH')) exit; // Exit if accessed directly

use Aelia\WC\CurrencySwitcher\ShippingPricing\Definitions;
use Aelia\WC\CurrencySwitcher\ShippingPricing\WC_Aelia_CS_ShippingPricing_Plugin;
use \WC_Aelia_CurrencySwitcher;

/**
 * A template class that will be used to extend the shipping methods to handle
 * multiple currencies. The class will be parsed using an eval() statement, after
 * having been modified to extend the target shipping method class.
 *
 * Example
 * Target class: WC_Shipping_Flat_Rate
 * New class declaration: Aelia_WC_Shipping_Flat_Rate extends WC_Shipping_Flat_Rate
 *
 * @see Aelia\WC\CurrencySwitcher\ShippingPricing\WC_Aelia_CS_ShippingPricing_Plugin::generate_shipping_method_class().
 */
class Aelia_Shipping_Method_Template {
	// @var string Keeps track of the active currency.
	protected $_active_currency;
	// @var array The default settings stored for the shipping method.
	protected $default_settings;
	// @var string The text domain to use for localisation.
	protected $text_domain;

	// @var bool Indicates if the manual prices for the active currencies are enabled.
	// When set to false, it means that the default shipping prices should be taken instead and
	// converted using exchange rates
	protected $manual_prices_enabled = false;
	// @var bool Indicates if the shipping prices loaded by the class are already in the active currency.
	public $shipping_prices_in_currency = false;

	/**
	 * Returns the instance of the Currency Switcher.
	 *
	 * @return WC_Aelia_CurrencySwitcher
	 */
	protected function cs() {
		return WC_Aelia_CurrencySwitcher::instance();
	}

	/**
	 * Returns the instance of the Shipping Pricing plugin.
	 *
	 * @return WC_Aelia_CS_ShippingPricing_Plugin
	 */
	protected function sp() {
		return WC_Aelia_CS_ShippingPricing_Plugin::instance();
	}

	/**
	 * Returns the base currency configured in WooCommerce.
	 *
	 * @return string
	 */
	protected function base_currency() {
		return WC_Aelia_CurrencySwitcher::settings()->base_currency();
	}

	/**
	 * Generates the settings key for a specific currency.
	 *
	 * @param string currency The target currency.
	 * @return string
	 */
	protected function get_settings_key($currency) {
		$base_settings_key = $this->plugin_id . $this->id . '_settings';
		return $this->get_option_key($base_settings_key, $currency);
	}

	/**
	 * Generates the settings key for a the specified option.
	 *
	 * @param string option The target option.
	 * @param string currency The target currency.
	 * @return string
	 */
	protected function get_option_key($option, $currency) {
		$settings_key = $option;
		if(!empty($currency)) {
			$settings_key .= '_' . $currency;
		}
		return $settings_key;
	}

	/**
	 * Determines the active currency, which will be used to load the appropriate
	 * settings.
	 *
	 *
	 * @return string
	 */
	protected function active_currency() {
		if(empty($this->active_currency)) {
			// On the backend, the active currency is the one selected explicitly on
			// the shipping method's settings page
			if(is_admin() && !WC_Aelia_CS_ShippingPricing_Plugin::doing_ajax()) {
				$currency = get_value(Definitions::ARG_SHIPPING_PRICING_CURRENCY, $_GET);

				if(!empty($currency) && $this->cs()->is_valid_currency($currency)) {
					$this->active_currency = $currency;
				}
				else {
					$this->active_currency = $this->base_currency();
				}
			}
			else {
				$this->active_currency = get_woocommerce_currency();
			}
		}
		return $this->active_currency;
	}

	/**
	 * Retrieves the default settings for the shipping method. The default settings
	 * are the ones saved by WooCommerce when the Shipping Pricing plugin is not
	 * enabled, and are not associated to any currency.
	 *
	 * @return array
	 */
	protected function get_default_settings() {
    if(empty($this->default_settings) || !is_array($this->default_settings)) {
			// Load default form_field settings
			$this->default_settings = get_option($this->plugin_id . $this->id . '_settings', null);
			$this->default_settings[Definitions::FIELD_SHIPPING_PRICING_CURRENCY] = $this->base_currency();
		}
		return is_array($this->default_settings) ? $this->default_settings : array();
	}

	/**
	 * Retrieves the settings for the shipping method in base currency.
	 *
	 * @return array
	 */
	protected function get_base_currency_settings() {
    if(empty($this->base_currency_settings) || !is_array($this->base_currency_settings)) {
			// Load default form_field settings
			$this->base_currency_settings = get_option($this->get_settings_key($this->base_currency()), null);
		}
		return is_array($this->base_currency_settings) ? $this->base_currency_settings : array();
	}

	/**
	 * Indicates if the shipping method is enabled.
	 *
	 * @return bool
	 */
	protected function is_enabled() {
		return $this->enabled == 'yes';
	}

	/**
	 * Loads the scripts and CSS for the shipping method settings page.
	 */
	protected function load_settings_page_scripts() {
		echo '<script src="' . $this->sp()->url('plugin') . '/js/admin/shipping_method.js' . '"></script>';
	}

	/**
	 * __construct function.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		parent::__construct();

		$this->method_title .= ' ' . '(CS)';
		$this->text_domain = WC_Aelia_CS_ShippingPricing_Plugin::$text_domain;
	}

	/**
	 * Stores the settings for the base currency. Mainly used to initialise the
	 * settings for base currency the first time the plugin is used to configure
	 * them.
	 *
	 * @param array settings An array of settings.
	 */
	protected function init_settings_for_base_currency($settings) {
		update_option($this->get_settings_key($this->base_currency()), $settings);
	}

	/**
	 * Loads the settings for the gateway, using the specigied key.
	 *
	 * @param string settings_key The key to use when loading the settings.
	 */
	protected function load_settings($settings_key) {
		// Load base currency settings (some of them apply to all currencies).
		$base_currency_settings = $this->get_base_currency_settings();

		// Load form_field settings
		$this->settings = get_option($settings_key, null);

		if(!$this->settings || !is_array($this->settings)) {
			if($this->active_currency() == $this->base_currency()) {
				// For base currency, when settings are empty, fetch and clone the
				// default ones
				$this->settings = $this->get_default_settings();
				$this->init_settings_for_base_currency($this->settings);
			}
			else {
				// For all other currencies, follow the standard logic
				$this->settings = array();
				// If there are no settings defined, load defaults
				if($form_fields = $this->get_form_fields()) {
					foreach($form_fields as $k => $v) {
						$this->settings[$k] = isset($v['default']) ? $v['default'] : '';
					}
				}
			}
		}

		if($this->settings && is_array($this->settings)) {
			$this->settings = array_map(array($this, 'format_settings'), $this->settings);

			// $this->settings['enabled'] indicates if the pricing entered for this
			// currency should be used.
			if(isset($this->settings['enabled']) && ($this->settings['enabled'] == 'yes') &&
				 ($this->settings['currency'] == $this->active_currency())) {
				$this->manual_prices_enabled = true;
			}
			else {
				$this->manual_prices_enabled = false;
			}
		}

		// $base_currency_settings['enabled'] indicates if the shipping method
		// itself is enabled. If it's not enabled, it won't be possible to use it,
		// regardless of any other settings
		$this->enabled = isset($base_currency_settings['enabled']) && $base_currency_settings['enabled'] == 'yes' ? 'yes' : 'no';
		$this->settings['enabled'] = $this->enabled;
	}

	/**
	 * Loads settings for the shipping method. This method takes into account the
	 * active currency and loads the appropriate settings, rather than the default
	 * ones.
	 */
	public function init_settings() {
		$settings_key = $this->get_settings_key($this->active_currency());
		$this->load_settings($settings_key);

		// If viewing the settings page for this shipping method, don't do anything
		// else and keep the settings as they are
		if(!$this->is_enabled() ||
			 (is_admin() && WC_Aelia_CS_ShippingPricing_Plugin::processing_settings())) {
			return;
		}

		// If we reach this point, we are not viewing the settings page for the shipping
		// method. In such case, we need to determine if we should use the pricing
		// for the specific currency, or fall back to the default pricing.
		// If this shipping method is not enabled, it means that the admins unticked
		// the "enabled" field for the shipping prices in the active currency. In
		// such case, the prices should be ignored and that the default settings
		// should be taken instead, to be converted using exchange rates
		if(!$this->manual_prices_enabled) {
			// Load and store the default settings (i.e. the ones that would be used
			// normally
			$this->load_settings($this->get_settings_key($this->base_currency()));
			$this->shipping_prices_in_currency = false;
		}
		else {
			$this->shipping_prices_in_currency = true;
		}
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 */
	public function init_form_fields() {
		parent::init_form_fields();
		$this->form_fields[Definitions::FIELD_SHIPPING_PRICING_CURRENCY] = array(
			'title' => '',
			'type' => 'hidden',
			'label' => null,
			'class' => 'currency hidden',
			'default' => $this->active_currency(),
			'custom_attributes' => array(
				'readonly' => 'readonly',
			),
		);

		// Change the description for the "enabled" field when a non-base currency is
		// active. For such currencies, the "enabled" field does not disable the
		// shipping method, it simply ignores the prices entered manually
		if($this->active_currency() != $this->base_currency()) {
			$original_enabled_field_label = $this->form_fields['enabled']['label'];

			$this->form_fields['enabled'] = array_merge($this->form_fields['enabled'], array(
				'label' => __('Enable manual prices in this currency', $this->text_domain),
				'description' => sprintf(__('If ticked, the prices entered in this page will be ' .
																		'used for this shipping method, when %s is the selected ' .
																		'currency. If unticked, the prices below will be ignored ' .
																		'the one configured for the base currency will be converted ' .
																		'automatically, using exchange rates.', $this->text_domain),
																 $this->active_currency()) .
												 '<br />' .
												 sprintf(__('If you wish to <strong>completely disable</strong> this ' .
																		'shipping method, please select the base currency (%s) ' .
																		'above, and untick the "<strong>%s</strong>" option.', $this->text_domain),
																 $this->base_currency(),
																 $original_enabled_field_label),
			));
		}
	}

	/**
	 * Adds a shipping rate. If taxes are not set they will be calculated based on
	 * cost.
	 *
	 * @param array args
	 */
	public function add_rate($args = array()) {
		parent::add_rate($args);

		// If prices have been loaded for the active currency, flag the rates
		// so that the prices won't be converted by the Currency Switcher
		if($this->shipping_prices_in_currency) {
			foreach($this->rates as $idx => $rate) {
				$rate->shipping_prices_in_currency = true;
				$this->rates[$idx] = $rate;
			}
		}
	}

	/**
	 * Renders the currency selector, to configure shipping pricing in another
	 * currency.
	 */
	protected function render_currency_selector() {
		global $wp;
		$url_args = $_GET;
		?>
		<div class="currency_selector">
			<div class="title"><?php
				echo __('Configuring prices for currency:', $this->text_domain) . ' ' . $this->active_currency();
				if($this->active_currency() == $this->base_currency()) {
					echo ' ' . __('(base currency)', $this->text_domain);
				}
			?></div>
			<div class="selectors">
				<span class="label"><?php
				echo __('Select a currency to configure the shipping prices:', $this->text_domain);
				?></span><?php

				$enabled_currencies = $this->cs()->enabled_currencies();
				asort($enabled_currencies);
				foreach($enabled_currencies as $currency) {
					$url_args[Definitions::ARG_SHIPPING_PRICING_CURRENCY] = $currency;
					$currency_change_url = $_SERVER['PHP_SELF'] . '?' . http_build_query($url_args);

					echo '<a class="currency_link" href="' . $currency_change_url . '">' . $currency. '</a>';
				}
			?></div>
		</div>
		<?php
	}

	/**
	 * Renders the settings screen.
	 */
	public function admin_options() {
		$this->load_settings_page_scripts();
		?>
		<div class="aelia shipping_method_settings">
			<h3><?php echo (!empty($this->method_title)) ? $this->method_title : __('Settings', 'woocommerce') ; ?></h3>
			<?php echo (!empty($this->method_description)) ? wpautop($this->method_description) : ''; ?>

			<?php $this->render_currency_selector(); ?>

			<table class="form-table">
				<?php $this->generate_settings_html(); ?>
			</table>
		</div>
		<?php
	}

	/**
	 * Processes the settings for the shipping method, saving them to the database.
	 */
	public function process_admin_options() {
		$this->validate_settings_fields();

		if(count($this->errors) > 0) {
			$this->display_errors();
			return false;
		}
		else {
			// When settings are saved, determine for which currency they are
			$currency_field_key = $this->plugin_id . $this->id . '_' . Definitions::FIELD_SHIPPING_PRICING_CURRENCY;
			if(isset($_POST[$currency_field_key])) {
				$settings_currency = $_POST[$currency_field_key];
			}
			update_option($this->get_settings_key($settings_currency), apply_filters('woocommerce_settings_api_sanitized_fields_' . $this->id, $this->sanitized_fields));
			$this->init_settings();
			return true;
		}
	}

	/**
	 * Indicates if class should load settings entered for base currency, rather
	 * than the ones explicitly entered for the active currency.
	 *
	 * @return bool
	 */
	protected function should_load_base_currency_settings() {
		// If viewing the settings page for this shipping method, don't do anything
		// else and keep the settings as they are
		if(is_admin() && WC_Aelia_CS_ShippingPricing_Plugin::processing_settings()) {
			return false;
		}

		// If we reach this point, we need to determine if we should use the flat
		// rates for the specific currency (when manual prices are enabled), or fall
		// back to the default pricing, to be converted using exchange rates
		return !$this->manual_prices_enabled;
	}

	/*** Rendering methods ***/
	/**
	 * Renders a hidden field.
	 *
	 * @param string key The field name.
	 * @param array data An array of parameters to render the field.
	 * @see WC_Settings_API::generate_text_html().
	 */
  public function generate_hidden_html($key, $data) {
    $field = $this->plugin_id . $this->id . '_' . $key;
    $defaults = array(
			'title' => '',
			'class' => '',
			'css' => '',
			'disabled' => false,
			'placeholder' => '',
			'type' => 'hidden',
			'desc_tip' => false,
			'description' => '',
			'custom_attributes' => array()
		);

		$data = wp_parse_args($data, $defaults);

		ob_start();
		?>
		<tr valign="top">
			<th scope="row" class="titledesc">
				<label for="<?php echo esc_attr($field); ?>"><?php echo wp_kses_post($data['title']); ?></label>
				<?php echo $this->get_tooltip_html($data); ?>
			</th>
			<td class="forminp">
				<fieldset>
					<legend class="screen-reader-text"><span><?php echo wp_kses_post($data['title']); ?></span></legend>
					<input class="input-text regular-input <?php echo esc_attr($data['class']); ?>" type="<?php echo esc_attr($data['type']); ?>" name="<?php echo esc_attr($field); ?>" id="<?php echo esc_attr($field); ?>" style="<?php echo esc_attr($data['css']); ?>" value="<?php echo esc_attr($this->get_option($key)); ?>" placeholder="<?php echo esc_attr($data['placeholder']); ?>" <?php disabled($data['disabled'], true); ?> <?php echo $this->get_custom_attribute_html($data); ?> />
					<?php echo $this->get_description_html($data); ?>
				</fieldset>
			</td>
		</tr>
		<?php
		return ob_get_clean();
  }
}
