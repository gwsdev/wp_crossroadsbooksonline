WooCommerce Currency Switcher - Shipping Pricing
===
**Tags**: woocommerce, currency switcher, shippinh, manual pricing
**Requires at least**: 3.6
**Tested up to**: 3.9.1

Extends the [Aelia Currency Switcher](http://aelia.co/shop/currency-switcher-woocommerce/) plugin, by allowing to manually specify prices for the various shipping options.

Description
---
This improves the [Aelia Currency Switcher](http://aelia.co/shop/currency-switcher-woocommerce/) plugin, by adding the possibility of specifying shipping prices manually, for each currency, rather than having them calculated on the fly using exchange rates.

**Requirements**

* PHP 5.3+
* WordPress 3.6+
* WooCommerce 2.1.x/2.2.x/2.3.x
* Aelia Currency Switcher 3.3.11.140619 or later
* [AFC plugin for WooCommerce](http://aelia.co/downloads/wc-aelia-foundation-classes.zip) 1.0.10.140819 or later.

Installation
---
1. Extract the zip file and drop the contents in the wp-content/plugins/ directory of your WordPress installation.
2. Activate the plugin through the 'Plugins' menu in WordPress.
3. That's it! Now the price of the Subscriptions you enter will be converted automatically in the appropriate currency.

Change log
---
Please refer to file **CHANGELOG.md** included with this plugin.
